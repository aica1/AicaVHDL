library ieee;
use ieee.std_logic_1164.all;

use work.core_pkg.all;

-- *****************************************************************************
entity aica_top is
  generic(
    USE_PREDICTOR : boolean := false  -- enable branch predictor
  );
  port(
    clk_i           : in  std_logic;    -- clock input
    rst_i           : in  std_logic;    -- reset input
    irq_i           : in  std_logic;    -- interrupt request
    irq_ack_o       : out std_logic;    -- interrupt request aknowledge
    imem_wait_i     : in  std_logic;
    imem_req_o      : out std_logic;
    imem_addr_o     : out std_logic_vector(N_DATA-1 downto ILSB);
    imem_data_i     : in  std_logic_vector(N_DATA-1 downto 0);
    dmem_wait_i     : in  std_logic;
    dmem_req_o      : out std_logic;
    dmem_we_byte_o  : out std_logic_vector(3 downto 0);
    dmem_addr_o     : out std_logic_vector(N_DATA-1 downto 0);
    dmem_data_i     : in  std_logic_vector(N_DATA-1 downto 0);
    dmem_data_o     : out std_logic_vector(N_DATA-1 downto 0)
  );
end entity aica_top;

-- *****************************************************************************
architecture behavior of aica_top is

begin

  -- ***************
  i_core : entity work.core_top
  generic map(
    USE_PREDICTOR => USE_PREDICTOR
  )
  port map(
    rst_i          => rst_i,
    clk_i          => clk_i,
    irq_ext_i      => irq_i,
    irq_ack_o      => irq_ack_o,
    imem_req_o     => imem_req_o,
    dmem_req_o     => dmem_req_o,
    imem_wait_i    => imem_wait_i,
    dmem_wait_i    => dmem_wait_i,
    dmem_we_byte_o => dmem_we_byte_o,
    imem_addr_o    => imem_addr_o,
    imem_data_i    => imem_data_i,
    dmem_addr_o    => dmem_addr_o,
    dmem_data_i    => dmem_data_i,
    dmem_data_o    => dmem_data_o
  );

end architecture behavior;

