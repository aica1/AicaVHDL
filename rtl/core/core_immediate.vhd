library ieee;
use ieee.std_logic_1164.all;

use work.core_pkg.all;
use work.riscv_pkg.all;

-- *****************************************************************************
-- All opcodes are inside the riscv_pkg
-- The immediate signal is extended through its MSb, to a 32 bits data
-- Types of immediate:
-- -> I - type, Register to Immediate instructions;
-- -> S - type, Store instructions;
-- -> B - type, Conditional Branch instructions;
-- -> U - type, LUI and AUIPC instructions;
-- -> J - type, JAL instructions;
entity core_immediate is
  port(
    opcode_i    : in  std_logic_vector(4 downto 0);       -- opcode_i field from the instruction
    imm_field_i : in  std_logic_vector(N_DATA-1 downto 7);-- all instr. fields with possible immediate bits
    imm_o       : out std_logic_vector(N_DATA-1 downto 0) -- immediate output
  );
end entity core_immediate;

-- *****************************************************************************
architecture behavior of core_immediate is
begin

  with opcode_i select
    imm_o(31 downto 20) <=
      imm_field_i(31 downto 20)   when OPCODE_LUI | OPCODE_AUIPC,  -- U_type
      (others => imm_field_i(31)) when others;  -- J_type ; I_type ; B_type ; S_type

  with opcode_i select
    imm_o(19 downto 12) <=
    imm_field_i(19 downto 12)   when OPCODE_LUI | OPCODE_AUIPC | OPCODE_JAL, -- U_type ; J_type
    (others => imm_field_i(31)) when others;  -- I_type ; B_type ; S_type

  with opcode_i select
    imm_o(11)	<=
      '0'             when OPCODE_LUI | OPCODE_AUIPC, -- U_type
      imm_field_i(7)  when OPCODE_BRANCH,             -- B_type
      imm_field_i(31) when others;                    -- I_type ; S_type

  with opcode_i select
    imm_o(10 downto 5) <=
      (others => '0')           when OPCODE_LUI | OPCODE_AUIPC, -- U_type
      imm_field_i(30 downto 25) when others;                    -- I_type ; B_type ; S_type ; J_type

  with opcode_i select
    imm_o(4 downto 1) <=
      (others => '0')           when OPCODE_LUI | OPCODE_AUIPC,   -- U_type
      imm_field_i(11 downto 8)  when OPCODE_BRANCH | OPCODE_STORE,-- B_type ; S_type
      imm_field_i(24 downto 21) when others;                      -- I_type ; J_type

  with opcode_i select
    imm_o(0) <=
      imm_field_i(20) when OPCODE_JALR | OPCODE_OPIMM | OPCODE_LOAD | OPCODE_SYSTEM,-- I_type
      imm_field_i(7)  when OPCODE_STORE, -- S_type
      '0'             when others;       -- U_type , B_type , J_type

end architecture behavior;
