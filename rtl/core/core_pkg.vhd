library ieee;
use ieee.std_logic_1164.all;

use work.riscv_pkg.all;

-- *****************************************************************************
package core_pkg is

  -- ***********************************
  -- Architecture (avoid changes)
  -- ***********************************
  constant N_DATA : integer := XLEN;  -- word size for data used inside the core
  constant ILSB   : integer := 1;     -- instruciton LSBit used for aligned access

  -- ***********************************
  -- CSR enconding
  -- The opcode is sightly different from the
  -- ISA manual, since there is some reuse from the
  -- immediate signals
  -- ***********************************
  -- CSR encoding
  constant CSR_MSTATUS_OP : std_logic_vector(3 downto 0) := "0000";
  constant CSR_MIE_OP     : std_logic_vector(3 downto 0) := "0100";
  constant CSR_MTVEC_OP   : std_logic_vector(3 downto 0) := "0101";
  constant CSR_MEPC_OP    : std_logic_vector(3 downto 0) := "1001";
  constant CSR_MCAUSE_OP  : std_logic_vector(3 downto 0) := "1010";

  -- ***********************************
  -- ALU opcode
  -- ***********************************
  -- Simplified encoding with the most used operations
  constant ENC_ALU_MISR   : std_logic_vector(1 downto 0) := "00";
  constant ENC_ALU_MISI   : std_logic_vector(1 downto 0) := "01";
  constant ENC_ALU_BRANCH : std_logic_vector(1 downto 0) := "10";
  constant ENC_ALU_ADD    : std_logic_vector(1 downto 0) := "11";

  -- *************************************
  -- Pipeline info => group pipeline register signals with 'record'
  -- NOTE: always add/remove signals from the reset function(s)
  --       on change
  -- *************************************
  -- IF/ID Pipeline Register
  type t_pipe_id is record
    bht_state   : std_logic_vector(2 downto 0);           -- state read from the branch predictor
    valid       : std_logic;                              -- pipeline valid flag
    pc          : std_logic_vector(N_DATA-1 downto ILSB); -- PC used to fetch the instruction
    instr       : std_logic_vector(N_DATA-1 downto 0);    -- instruction
  end record t_pipe_id;

  -- ID/EX Pipeline Register
  type t_pipe_ex is record
    bht_state     : std_logic_vector(2 downto 0); -- state read from the branch predictor
    valid         : std_logic;                    -- pipeline valid flag
    regfile_we    : std_logic;
    regfile_sel0  : std_logic;
    branch_unc    : std_logic;
    branch        : std_logic;
    branch_sel    : std_logic;
    mread         : std_logic;                    -- memory read
    mwrite        : std_logic;                    -- memory write
    csr_en        : std_logic;
    excp_instr    : std_logic;
    pc_arg_sel    : std_logic;
    alu_op        : std_logic_vector(1 downto 0);
    alu_sela      : std_logic;                          -- 0 = pc; 1 = data_rs1
    alu_selb      : std_logic;                          -- 0 = data_rs2; 1 = immediate
    pc            : std_logic_vector(N_DATA-1 downto ILSB);
    data_rs1      : std_logic_vector(N_DATA-1 downto 0);
    data_rs2      : std_logic_vector(N_DATA-1 downto 0);
    imm           : std_logic_vector(N_DATA-1 downto 0);
    funct3        : std_logic_vector(2 downto 0);
    rs1           : std_logic_vector(N_XLEN-1 downto 0);
    rs2           : std_logic_vector(N_XLEN-1 downto 0);
    rd            : std_logic_vector(N_XLEN-1 downto 0);
  end record t_pipe_ex;

  -- EX/WB Pipeline Register
  type t_pipe_wb is record
    bht_state     : std_logic_vector(2 downto 0);
    regfile_we    : std_logic;
    regfile_sel0  : std_logic;
    branch_unc    : std_logic;
    branch        : std_logic;
    branch_sel    : std_logic;          -- branch select
    mread         : std_logic;          -- memory read
    mwrite        : std_logic;          -- memory write
    excp_en       : std_logic;
    irq           : std_logic;
    excp_mem      : std_logic;
    excp_instr    : std_logic;
    alt_result    : std_logic_vector(N_DATA-1 downto 0);  -- alternative result from ALU
    pc            : std_logic_vector(N_DATA-1 downto ILSB);
    alu_result    : std_logic_vector(N_DATA-1 downto 0);
    alu_zero      : std_logic;
    funct3        : std_logic_vector(2 downto 0);
    rs2           : std_logic_vector(N_XLEN-1 downto 0);
    rd            : std_logic_vector(N_XLEN-1 downto 0);
  end record t_pipe_wb;

  -- Data from stage WB to stage IF
  type t_wb2if is record
    bht_state     : std_logic_vector(2 downto 0);
    branch        : std_logic;
    pc_sel        : std_logic;
    branch_addr   : std_logic_vector(N_DATA-1 downto ILSB);
    pc            : std_logic_vector(N_DATA-1 downto ILSB);
  end record t_wb2if;

  -- Data from stage EX to stage ID
  type t_ex2id is record
    mread         : std_logic;
    rd            : std_logic_vector(N_XLEN-1 downto 0);
  end record t_ex2id;

  -- Data from stage WB to stage ID
  type t_wb2id is record
    regfile_we    : std_logic;
    data_rd       : std_logic_vector(N_DATA-1 downto 0);
    rd            : std_logic_vector(N_XLEN-1 downto 0);
  end record t_wb2id;

  -- set of functions used to get the rst value of each pipeline register
  function rstRegisterID return t_pipe_id;
  function rstRegisterEX return t_pipe_ex;
  function rstRegisterWB return t_pipe_wb;

end package core_pkg;

-- *****************************************************************************
package body core_pkg is

  -- ***********************************
  -- Get reset value for the Stage ID pipeline register.
  -- ***********************************
  function rstRegisterID return t_pipe_id is
    variable temp : t_pipe_id;
  begin
    temp.bht_state  := (others => '0');
    temp.valid      := '0';
    temp.pc         := (others => '0');
    temp.instr      := RV32I_NOP;

    return temp;
  end function;

  -- ***********************************
  -- Get reset value for the Stage EX pipeline register.
  -- ***********************************
  function rstRegisterEX return t_pipe_ex is
    variable temp : t_pipe_ex;
  begin
    temp.bht_state    := (others => '0');
    temp.valid        := '0';
    temp.regfile_we   := '0';
    temp.regfile_sel0 := '0';
    temp.branch_unc   := '0';
    temp.branch       := '0';
    temp.branch_sel   := '0';
    temp.mread        := '0';
    temp.mwrite       := '0';
    temp.csr_en       := '0';
    temp.excp_instr   := '0';
    temp.pc_arg_sel   := '0';
    temp.alu_op       := (others => '0');
    temp.alu_sela     := '0';
    temp.alu_selb     := '0';
    temp.pc           := (others => '0');
    temp.data_rs1     := (others => '0');
    temp.data_rs2     := (others => '0');
    temp.imm          := (others => '0');
    temp.funct3       := (others => '0');
    temp.rs1          := (others => '0');
    temp.rs2          := (others => '0');
    temp.rd           := (others => '0');

    return temp;
  end function;

  -- ***********************************
  -- Get reset value for the Stage WB pipeline register.
  -- ***********************************
  function rstRegisterWB return t_pipe_wb is
    variable temp : t_pipe_wb;
  begin
    temp.bht_state     := (others => '0');
    temp.regfile_we    := '0';
    temp.regfile_sel0  := '0';
    temp.branch_unc    := '0';
    temp.branch        := '0';
    temp.branch_sel    := '0';
    temp.mread         := '0';
    temp.mwrite        := '0';
    temp.excp_en       := '0';
    temp.irq           := '0';
    temp.excp_mem      := '0';
    temp.excp_instr    := '0';
    temp.alt_result    := (others => '0');
    temp.pc            := (others => '0');
    temp.alu_result    := (others => '0');
    temp.alu_zero      := '0';
    temp.funct3        := (others => '0');
    temp.rs2           := (others => '0');
    temp.rd            := (others => '0');

    return temp;
  end function;

end package body core_pkg;
