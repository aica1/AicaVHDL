library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.riscv_pkg.all;

-- *****************************************************************************
entity core_csr is
  port(
    rst_i         : in  std_logic;
    clk_i         : in  std_logic;
    -- stall/flush flags
    flush_i       : in  std_logic;
    flush_excp_i  : in  std_logic;
    -- ctrl signals
    csr_en_i      : in  std_logic;
    csr_op_i      : in  std_logic_vector(2 downto 0);
    csr_sel_i     : in  std_logic_vector(3 downto 0);
    -- type of exception
    excp_daddr_i  : in  std_logic;                              -- bad data address (load/store)
    excp_instr_i  : in  std_logic;                              -- illegal instr. (instr[1:0] < "11")
    irq_i         : in  std_logic;                              -- external interrupt request
    -- input and output data for CSR registers
    mepc_i        : in  std_logic_vector(N_DATA-1 downto ILSB); -- mepc input
    mtvec_base_o  : out std_logic_vector(N_DATA-1 downto ILSB); -- mtvec base address
    irq_en_o      : out std_logic;                              -- interrupt enable
    excp_en_o     : out std_logic;                              -- software exception enable
    csr_data_i    : in  std_logic_vector(N_DATA-1 downto 0);
    csr_data_o    : out std_logic_vector(N_DATA-1 downto 0)
  );
end entity core_csr;

-- *****************************************************************************
architecture behavior of core_csr is

  signal data_read : std_logic_vector(N_DATA-1 downto 0);
  signal en        : std_logic;

  signal new_data  : std_logic_vector(N_DATA-1 downto 0);

  -- machine (M) register
  signal r_mstatus  : std_logic_vector(1 downto 0) := "01";   -- machine level status reg only uses 2 bits, [4] and [0]
  signal r_mie      : std_logic_vector(1 downto 0);
  signal r_mtvec    : std_logic_vector(N_DATA-1 downto 0);    -- trap handler base address
  signal r_mepc     : std_logic_vector(N_DATA-1 downto ILSB); -- exception program counter
  signal r_mcause   : std_logic_vector(4 downto 0);           -- exception/interrupt cause
  
  alias  mstatus_mpie : std_logic is r_mstatus(1);
  alias  mstatus_mie  : std_logic is r_mstatus(0);
  alias  mtvec_base   : std_logic_vector(N_DATA-1 downto ILSB) is r_mtvec(N_DATA-1 downto ILSB);
  alias  mtvec_mode   : std_logic_vector(1 downto 0) is r_mtvec(1 downto 0);
  alias  mcause_id    : std_logic_vector(3 downto 0) is r_mcause(3 downto 0);

  signal tvec_offset : std_logic_vector(3 downto 0); -- select offset added to the mtvec_base

begin

  irq_en_o  <= r_mie(1) and mstatus_mie;
  excp_en_o <= r_mie(0) and mstatus_mie;

  en <= '1' when (flush_i = '0' and csr_en_i = '1') else '0';

  -- select input data for the CSR
  with csr_op_i(1 downto 0) select
    new_data <=
      csr_data_i                      when "01",
      data_read or csr_data_i         when "10",  -- set bit mask
      data_read and (not(csr_data_i)) when "11",  -- clear bit mask
      (others => '-')                 when  others;

  -- Output
  with csr_sel_i select
    data_read <=
      (7 => mstatus_mpie, 3 => mstatus_mie, others => '0')                    when  CSR_MSTATUS_OP,
      r_mtvec                                                                 when  CSR_MTVEC_OP,
      r_mepc&"0"                                                              when  CSR_MEPC_OP,
      (31 => r_mcause(4), 3 downto 0 => r_mcause(3 downto 0), others => '0')  when  CSR_MCAUSE_OP,
      (11 => r_mie(1), 3 => r_mie(0), others => '0')                          when  CSR_MIE_OP,
      (others => '-') when  others;

  csr_data_o <= data_read;

  -- ***********************************
  -- Machine Status
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_mstatus <= (others => '0');

      elsif (flush_excp_i = '1') then -- disable global traps
        mstatus_mpie <= mstatus_mie;
        mstatus_mie  <= '0';

      elsif (en = '1' and csr_op_i = "000") then -- enable global traps again
        mstatus_mpie <= '0';
        mstatus_mie  <= mstatus_mpie;

      elsif (en = '1' and csr_sel_i = CSR_MSTATUS_OP) then
        r_mstatus <= new_data(7) & new_data(3);

      end if;
    end if;
  end process;

  -- ***********************************
  -- Machine Interrupt Enable
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_mie <= (others => '0');
      elsif (en = '1' and csr_sel_i = CSR_MIE_OP) then
        r_mie <= new_data(11) & new_data(3);
      end if;
    end if;
  end process;

  -- ***********************************
  -- Machine Trap Handler Base Address
  tvec_offset <=
    mcause_id when (mtvec_mode = CSR_TVEC_VECTOR_MODE and flush_excp_i = '1' and irq_i = '1') else
    (others => '0');

  mtvec_base_o <= std_logic_vector(unsigned(mtvec_base) + unsigned(tvec_offset));

  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_mtvec <= (others => '0');

      elsif (en = '1' and csr_sel_i = CSR_MTVEC_OP) then
        r_mtvec <= new_data(N_DATA-1 downto 0);

      end if;
    end if;
  end process;

  -- ***********************************
  -- Machine Exception Program Counter
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_mepc <= (others => '0');

      elsif (flush_excp_i = '1') then
        r_mepc <= mepc_i;

      elsif (en = '1' and csr_sel_i = CSR_MEPC_OP) then
        r_mepc <= new_data(N_DATA-1 downto ILSB);

      end if;
    end if;
  end process;

  -- ***********************************
  -- MCAUSE : Machine Cause
  -- cause of the exception => 0 = illegal instr; 2 = bad instr. address; 4 = bad data address
  -- cause of interrupt (machine mode only) => 3 = software; 7 = timer; 11 = external
  -- priority level: from software (most) => external (less)
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_i = '1') then
        r_mcause <= (others => '0');

      elsif (flush_excp_i = '1') then
        if (irq_i = '1') then         -- interrupt has priority
          r_mcause <= '1' & "1011";   -- "1011" is the code for Machine external interrupt;
        else
          r_mcause <= "00" & excp_daddr_i & "0" & excp_instr_i;
        end if;

      elsif (en = '1' and csr_sel_i = CSR_MCAUSE_OP) then
        r_mcause <= new_data(N_DATA-1) & new_data(3 downto 0);

      end if;
    end if;
  end process;

end architecture behavior;
