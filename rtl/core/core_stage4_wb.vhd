library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;

-- *****************************************************************************
entity core_stage4_wb is
  port(
    rst_i           : in  std_logic;
    clk_i           : in  std_logic;
    flush_i         : in  std_logic;                              -- flush flag
    stall_i         : in  std_logic;                              -- pipeline stall
    -- stage pipeline register, EX/WB
    dmem_data_i     : in  std_logic_vector(N_DATA-1 downto 0);    -- data from data memory
    pc_sel_o        : out std_logic;                              -- select pc source at IF stage
    branch_addr_o   : out std_logic_vector(N_DATA-1 downto ILSB); -- branch target address
    data_rd_o       : out std_logic_vector(N_DATA-1 downto 0);    -- data rd from this stage (forwarding)
    pipe_wb_i       : in  t_pipe_wb
  );
end entity core_stage4_wb;

-- *****************************************************************************
architecture behavior of core_stage4_wb is

    -- Branch unit signals
  signal zero_valid   : std_logic;
  signal slt_valid    : std_logic;
  signal branch_valid : std_logic;

  -- Load unit signals
  signal load_ext   : std_logic;                      -- bit ext, used with load half/byte
  signal load_upper : std_logic_vector(31 downto 16); -- 2 MSByte from the load data
  signal load_lower : std_logic_vector(15 downto 0);  -- 2 LSByte from the load data

begin

  -- MUX: define the target address
  branch_addr_o <=
    pipe_wb_i.alt_result(N_DATA-1 downto ILSB) when (pipe_wb_i.branch_sel = '0') else
    pipe_wb_i.alu_result(N_DATA-1 downto ILSB);  -- when (exmem.branch_sel = '1');

  -- ***********************************
  -- Branch Unit: decode a branch instruction type in to a:
  -- Branch if equal, Branch if not equal, Branch if greater
  -- signed/unsigned, and Branch if less signed/unsigned

  zero_valid   <= (pipe_wb_i.alu_zero xor pipe_wb_i.funct3(0) ) and not(pipe_wb_i.funct3(2));
  slt_valid    <= (pipe_wb_i.alu_result(0) xor pipe_wb_i.funct3(0) ) and pipe_wb_i.funct3(2);
  branch_valid <= zero_valid or slt_valid;

  -- control signal PC_Src, defines the MUX 0 (from stage1 IF) output
  pc_sel_o <= pipe_wb_i.branch_unc or (branch_valid and pipe_wb_i.branch);

  -- ***********************************
  -- Load Unit: decode a load instruction type in to a LOAD:
  -- Word, Half word signed, Half word unsigned,
  -- Byte signed or Byte unsigned.

  load_ext <=
    '0'             when (pipe_wb_i.funct3(2) = '1') else
    dmem_data_i(15) when (pipe_wb_i.funct3(0) = '1') else
    dmem_data_i(7)  when (pipe_wb_i.funct3(0) = '0') else
    '0';

  load_upper <=
    dmem_data_i(31 downto 16) when (pipe_wb_i.funct3(1) = '1') else
    (others => load_ext);

  load_lower(15 downto 8) <=
    dmem_data_i(15 downto 8) when (pipe_wb_i.funct3(1) = '1' or pipe_wb_i.funct3(0) = '1') else
    (others => load_ext);    --  when (exmem.funct3(0) = '0');

  load_lower(7 downto 0) <= dmem_data_i(7 downto 0);

  -- ***********************************
  -- result for the register file

  -- MUX 4 : define the register file data input
  data_rd_o <=
    load_upper&load_lower when (pipe_wb_i.mread = '1')        else
    pipe_wb_i.alt_result  when (pipe_wb_i.regfile_sel0 = '0') else
    pipe_wb_i.alu_result; -- when (exmem.regfile_sel0 = '1');

end architecture behavior;
