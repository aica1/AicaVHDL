# Descrição

Modelo de documento **tex** para a escrita de especificações do projeto **uC32RFBR**.

# Como Utilizar

Inicialmente copiamos todos os arquivos desse repositório, no commit mais atual da branch develop. A escrita do texto vai se dar principalmente nos arquivos .tex que encontramos dentro do diretório **text_src/**, visto que os demais são de configuração.

## Adicionando novos arquivos de texto
O arquivo main.tex realiza a configuração de alguns pacotes, e da classe utilizada pelo documento. Dentro de **text_src/chapters.tex** inserimos os arquivos separados por capítulos, por meio do comando \input{text_src/**<chapter_name>**.tex} Sendo esses arquivos inseridos os que criamos/modificamos para escrever o documento.

## Diretório de Figuras

Adicionar as figuras utilizadas pelo texto dentro do diretório **figure/**, com mais um subdiretório indicando o capítulo que a mesma é inserida, exemplo do adc: **figure/adc/adc.png**

## Snippets

Podemos utilizar esse diretório para inserir arquivos com trechos de código a serem inseridos no documento. O capítulo **"Exemplos de Programação"** desse template demonstra um comando que inseri um arquivo com uma função em C.

## Capítulo de um Periférico

Criar um capítulo para descrever um periférico (mesmo que o documento seja somente sobre ele, visto que facilita a unificação dos documentos mais adiante), e seguir o modelo de seções utilizadas nesse template para o Conversor A/D:

- Capítulo X  :  <Nome do Periférico>
- Seção    X.1: Descrição Funcional
- Seção    X.1.1: Resumo
- Seção    X.1.1: Diagrama De Blocos
- Seção    X.1.N: **outras subseções que descreverm o comportamento**
- Seção    X.2: Mapa de Registradores
- Seção    X.2.N: **descrição dos registradores e seus campos**
- Seção    X.3: Pinos
- Seção    X.4: Verificação

# Compilando

Dentro da raíz desse repositório encontramos um Makefile com comandos para gerar um pdf (no mesmo nível de diretório), considerando que LaTeX esteja instalado na máquina, e na variável PATH. Executando então o comando a seguir, geramos o arquivo **template.pdf**:

```
make pdf
```

Podemos trocar o nome do pdf gerado ao modificar a variável **PDF_FILE**, dentro do Makefile. Outros comandos:

- **make pdfbib** - compila o pdf, e gera a bibliográfia
- **make clean** - remove arquivos temporários criados durante a compilação

# Seguindo o Template

O diretório **common** contém os arquivos de configurações do documento, em conjunto com alguns comandos para a criação de tabelas, ou registradores de dados, entre outros. Sendo esse o principal diretório. Logo, caso o template tenha sido modificado, podemos simplesmente remover o diretório **common** que utilizamos atualmente, e copiar o mais recente encontrado nesse repositório, que seria do último commit na branch develop.

*Nota: após a atualização do diretório common alguns comandos podem gerar erros durante a compilação do documento, caso os argumentos tenham sido alterados.*

