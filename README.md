# Aica - Descrição VHDL

Descrição em VHDL do processador Aica de 32 bits, utilizando a ISA RISC-V, mais específicamente, a arquitetura RV32I.

TODO: precisa de atualização

_________________________
## Binários de Teste

Os binários de teste que são carregados na RAM durante as simulações são encontrados no diretório **./sw_bin**, em conjunto com o seu dump para debug. Para atualizar os arquivos executamos o seguinte comando na raiz do repositório:

```sh
make sw-update
```

_________________________
## Vivado

O diretório **./vivado** contém scripts para geração do projeto para o Vivado, sendo disponibilizado os comandos por meio do **Makefile** ali presente:

| Comando       | Descrição |
|---------------|---------  |
| `project-gui` | Cria um projeto do Vivado no modo tcl e o abre para uso imediato  |
| `project`     | Cria um projeto do Vivado no modo tcl                             |
| `clean`       | Remove os arquivos de projeto do disco                            |


Após a geração do projeto podemos simular o RTL com os seguintes conjutos predefinidos:

- **sim_1:** simulação de testbench com o módulo aica_tb.vhd definido como topo **(default)**
- **sim_vf:** simulação de verificação com o módulo vf_top.sv definido como topo
